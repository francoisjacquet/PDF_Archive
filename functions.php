<?php
/**
 * Functions
 *
 * @package PDF Archive
 */

// Register functions to be hooked, last, if plugins before this one alters the email.
add_action( 'functions/PDF.php|pdf_stop_pdf', 'PDFArchiveTriggered', 2 );
add_action( 'functions/PDF.php|pdf_stop_html', 'PDFArchiveTriggered', 2 );

// Triggered function.
function PDFArchiveTriggered( $hook_tag, $pdf_or_html )
{
	global $FileUploadsPath,
		$_ROSARIO;

	// "Print" PDF excluded?
	$is_print_pdf = isset( $_REQUEST['bottomfunc'] ) && $_REQUEST['bottomfunc'] === 'print';

	if ( ProgramConfig( 'pdf_archive', 'PDF_ARCHIVE_EXCLUDE_PRINT' )
		&& $is_print_pdf )
	{
		// Do NOT Archive PDF generated using the "Print" button.
		return false;
	}

	if ( ! User( 'STAFF_ID' ) && ! UserStudentID() )
	{
		return false;
	}

	$archive_programs = [];

	if ( User( 'PROFILE' ) )
	{
		$archive_programs = ProgramConfig( 'pdf_archive', 'PDF_ARCHIVE_PROGRAMS_' . User( 'PROFILE' ) );

		if ( $archive_programs )
		{
			$archive_programs = json_decode( $archive_programs, true );
		}
	}

	$program = ! empty( $_ROSARIO['ProgramLoaded'] ) ? $_ROSARIO['ProgramLoaded'] :
		( ! empty( $_REQUEST['modname'] ) ? $_REQUEST['modname'] : '' );

	if ( ! $program
		|| ! $archive_programs
		|| ! in_array( $program, (array) $archive_programs ) )
	{
		// Do NOT archive PDF for this program, at least for this profile.
		return false;
	}

	if ( ! $FileUploadsPath )
	{
		return false;
	}

	// assets/FileUploads/PDF_Archive/school_1/2024/01/
	$path = $FileUploadsPath . 'PDF_Archive/school_' . (int) UserSchool() . '/' . date( 'Y/m' ) . '/';

	if ( ! is_dir( $path )
		&& ! @mkdir( $path, 0755, true ) ) // Fix shared hosting: permission 755 for directories.
	{
		return false;
	}

	if ( ! is_writable( $path ) )
	{
		return false;
	}

	require_once 'ProgramFunctions/FileUpload.fnc.php';

	$filename = User( 'STAFF_ID' ) ? 'staff_' . User( 'STAFF_ID' ) : 'student_' . UserStudentID();

	$filename .= '_' . iconv(
		'UTF-8',
		// Fix PHP Notice iconv() Detected an illegal character in input string
		'ISO-8859-1//IGNORE',
		str_replace(
			[ _( 'Print' ) . ' ', ' ' ],
			[ '', '_' ],
			ProgramTitle()
		)
	);

	$filename = FileNameTimestamp( $filename );

	$is_pdf = $hook_tag === 'functions/PDF.php|pdf_stop_pdf';

	if ( $is_pdf )
	{
		$document_path = $path . $filename . '.pdf';

		$pdf_or_html->saveAs( $document_path );
	}
	else // Is HTML.
	{
		$document_path = $path . $filename . '.html';

		// Save.
		file_put_contents( $document_path, $pdf_or_html );
	}

	$pdf = [
		'STAFF_ID' => User( 'STAFF_ID' ) ? User( 'STAFF_ID' ) : '',
		'STUDENT_ID' => User( 'STAFF_ID' ) ? '' : UserStudentID(),
		'DOCUMENT_PATH' => $document_path,
		'PROGRAM_TITLE' => ProgramTitle(),
	];

	return DBInsert(
		'pdf_archive',
		$pdf + [ 'SCHOOL_ID' => (int) UserSchool() ]
	);
}
