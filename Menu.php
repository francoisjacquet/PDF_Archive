<?php
/**
 * Menu.php file
 * Required
 * - Menu entries for the PDF Archive module
 * - Add Menu entries to other modules
 *
 * @package PDF Archive module
 */

/**
 * Use dgettext() function instead of _() for Module specific strings translation
 * see locale/README file for more information.
 */
$module_name = dgettext( 'PDF_Archive', 'PDF Archive' );

// Menu entries for the School Setup module.
$menu['School_Setup']['admin']['PDF_Archive/PDFArchive.php'] = dgettext( 'PDF_Archive', 'PDF Archive' );
