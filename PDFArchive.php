<?php
/**
 * PDF Archive program
 *
 * @package PDF Archive
 */

require_once 'ProgramFunctions/FileUpload.fnc.php';

DrawHeader( ProgramTitle() );

// Set start date.
$start_date = RequestedDate( 'start', date( 'Y-m-d', time() - 60 * 60 * 24 ) );

// Set end date.
$end_date = RequestedDate( 'end', DBDate() );

if ( $_REQUEST['modfunc'] === 'update'
	&& $_REQUEST['tab'] === 'config'
	&& AllowEdit() )
{
	if ( ! empty( $_REQUEST['program_config'] ) )
	{
		foreach ( (array) $_REQUEST['program_config'] as $column => $value )
		{
			ProgramConfig(
				'pdf_archive',
				$column,
				$value
			);
		}
	}

	RedirectURL( [ 'modfunc', 'config' ] );
}

/*if ( $_REQUEST['modfunc'] === 'delete'
	&& AllowEdit() )
{
	// Prompt before deleting log.
	if ( DeletePrompt( dgettext( 'PDF_Archive', 'PDF Archive' ) ) )
	{
		DBQuery( 'DELETE FROM pdf_archive' );

		$note[] = dgettext( 'PDF_Archive', 'PDF Archive cleared.' );

		// Unset modfunc & redirect URL.
		RedirectURL( 'modfunc' );
	}
}*/

if ( count( $_REQUEST ) === 3
	&& ! $_REQUEST['modfunc']
	&& ProgramConfig( 'pdf_archive', 'PDF_ARCHIVE_DELETE_OLDER_THAN_DAYS' ) )
{
	// Only requested modname.
	$days_ago_date = date(
		'Y-m-d',
		strtotime( ( (int) ProgramConfig( 'pdf_archive', 'PDF_ARCHIVE_DELETE_OLDER_THAN_DAYS' ) ) . ' days ago' )
	);

	$pdf_to_delete_RET = DBGet( "SELECT DOCUMENT_PATH
		FROM pdf_archive
		WHERE CREATED_AT<'" . $days_ago_date . "'
		AND SCHOOL_ID='" . UserSchool() . "';" );

	foreach ( $pdf_to_delete_RET as $pdf_to_delete )
	{
		$pdf_path = $pdf_to_delete['DOCUMENT_PATH'];

		if ( function_exists( 'FileDelete' ) )
		{
			// Security: use FileDelete() instead of unlink()
			FileDelete( $pdf_path );
		}
		elseif ( $pdf_path
			&& file_exists( $pdf_path )
			&& is_writable( $pdf_path ) )
		{
			unlink( $pdf_path );
		}
	}

	// Automatically delete entries older than X days
	DBQuery( "DELETE FROM pdf_archive
		WHERE CREATED_AT<'" . $days_ago_date . "'
		AND SCHOOL_ID='" . UserSchool() . "';" );
}

echo ErrorMessage( $note, 'note' );

if ( ! $_REQUEST['modfunc']
	&& isset( $_REQUEST['tab'] )
	&& $_REQUEST['tab'] === 'config' )
{
	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=config&modfunc=update' ) . '" method="POST">';

	DrawHeader(
		'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] ) . '">« ' . _( 'Back' ) . '</a>',
		SubmitButton()
	);

	echo '<br>';

	$school_title = '';

	// If more than 1 school, add its title to table title.
	if ( SchoolInfo( 'SCHOOLS_NB' ) > 1 )
	{
		$school_title = SchoolInfo( 'SHORT_NAME' );

		if ( ! $school_title )
		{
			// No short name, get full title.
			$school_title = SchoolInfo( 'TITLE' );
		}

		$school_title = ' (' . $school_title . ')';
	}

	PopTable( 'header', _( 'Configuration' ) . $school_title );

	$delete_input = '<table class="cellspacing-0"><tr><td>' . sprintf(
		dgettext( 'PDF_Archive', 'Automatically delete entries older than %s days' ),
		'</td><td>' . TextInput(
			ProgramConfig( 'pdf_archive', 'PDF_ARCHIVE_DELETE_OLDER_THAN_DAYS' ),
			'program_config[PDF_ARCHIVE_DELETE_OLDER_THAN_DAYS]',
			'',
			'type="number" min="0" max="9999"'
		) . '</td><td>'
	) . '</td></tr></table>';

	$exclude_print_checkbox = CheckboxInput(
		ProgramConfig( 'pdf_archive', 'PDF_ARCHIVE_EXCLUDE_PRINT' ),
		'program_config[PDF_ARCHIVE_EXCLUDE_PRINT]',
		dgettext( 'PDF_Archive', 'Exclude PDF generated using the "Print" button' ),
		'',
		false,
		button( 'check' ),
		button( 'x' )
	);

	echo $delete_input . '<br>' .
		$exclude_print_checkbox;

	PopTable( 'footer' );

	echo '<br><div class="center">' . SubmitButton() . '</div></form>';
}

if ( isset( $_REQUEST['tab'] )
	&& $_REQUEST['tab'] === 'programs' )
{
	require_once 'modules/PDF_Archive/includes/Profiles.inc.php';
}

if ( ! $_REQUEST['modfunc']
	&& empty( $_REQUEST['tab'] ) )
{
	echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname']  ) . '" method="GET">';

	if ( ! AllowEdit() )
	{
		$_ROSARIO['allow_edit'] = true;

		$allow_edit_tmp = true;
	}

	DrawHeader(
		_( 'From' ) . ' ' . DateInput( $start_date, 'start', '', false, false ) . ' - ' .
		_( 'To' ) . ' ' . DateInput( $end_date, 'end', '', false, false ) .
		Buttons( _( 'Go' ) ),
		'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=config' ) . '">' . _( 'Configuration' ) . '</a> | ' .
		'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&tab=programs' ) . '">' . dgettext( 'PDF_Archive', 'Programs' ) . '</a>'
	);

	if ( ! empty( $allow_edit_tmp ) )
	{
		$_ROSARIO['allow_edit'] = false;
	}

	echo '</form>';

	// Format DB data.
	$pdf_archives_functions = [
		'CREATED_AT' => 'ProperDateTime',
		'FULL_NAME' => '_makePDFArchiveFullName',
		'DOCUMENT_PATH' => '_makePDFArchiveDocumentPath',
	];

	$pdf_archives_sql = "SELECT
		ID,pa.CREATED_AT,PROGRAM_TITLE,DOCUMENT_PATH,pa.STUDENT_ID,pa.STAFF_ID,
		" . DisplayNameSQL( 's' ) . " AS FULL_NAME,s.SYEAR AS STAFF_SYEAR,s.PROFILE,
		" . DisplayNameSQL( 'st' ) . " AS FULL_NAME_STUDENT
		FROM pdf_archive pa
		LEFT JOIN staff s ON (s.STAFF_ID=pa.STAFF_ID)
		LEFT JOIN students st ON (st.STUDENT_ID=pa.STUDENT_ID)
		WHERE (pa.SCHOOL_ID='" . UserSchool() . "' OR pa.SCHOOL_ID='0')
		AND pa.CREATED_AT>='" . $start_date . "'
		AND pa.CREATED_AT<='" . $end_date . ' 23:59:59' . "'
		ORDER BY pa.CREATED_AT DESC";

	if ( function_exists( 'SQLLimitForList' ) )
	{
		// @since RosarioSIS 11.7 Limit SQL result for List
		$sql_count = "SELECT COUNT(1)
			FROM pdf_archive pa
			LEFT JOIN staff s ON (s.STAFF_ID=pa.STAFF_ID)
			LEFT JOIN students st ON (st.STUDENT_ID=pa.STUDENT_ID)
			WHERE (pa.SCHOOL_ID='" . UserSchool() . "' OR pa.SCHOOL_ID='0')
			AND pa.CREATED_AT>='" . $start_date . "'
			AND pa.CREATED_AT<='" . $end_date . ' 23:59:59' . "'";

		$pdf_archives_sql .= SQLLimitForList( $sql_count );
	}
	else
	{
		$pdf_archives_sql .= " LIMIT 3000";
	}

	$pdf_archives_RET = DBGet( $pdf_archives_sql, $pdf_archives_functions );

	/*echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=delete' ) . '" method="POST">';

	DrawHeader( '', SubmitButton( _( 'Clear Archive' ), '', '' ) );

	echo '</form>';*/

	ListOutput(
		$pdf_archives_RET,
		[
			'CREATED_AT' => _( 'Date' ),
			'FULL_NAME' => dgettext( 'PDF_Archive', 'Generated by' ),
			'PROGRAM_TITLE' => dgettext( 'PDF_Archive', 'Program Title' ),
			'DOCUMENT_PATH' => dgettext( 'PDF_Archive', 'Document' ),
		],
		dgettext( 'PDF_Archive', 'PDF' ),
		dgettext( 'PDF_Archive', 'PDFs' ),
		[],
		[],
		[
			'count' => true,
			'save' => true,
			// Add pagination for list > 1000 results
			'pagination' => true,
			'valign-middle' => true,
		]
	);

}


/**
 * Make Document Path
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'DOCUMENT_PATH'.
 *
 * @return string
 */
function _makePDFArchiveDocumentPath( $value, $column )
{
	global $THIS_RET;

	if ( ! $value
		|| isset( $_REQUEST['LO_save'] ) )
	{
		return $value;
	}

	if ( ! file_exists( $value ) )
	{
		return button( 'x' );
	}

	$file_name = basename( $value );

	$file_size = HumanFilesize( filesize( $value ) );

	return button(
		'download',
		_( 'Download' ),
		'"' . URLEscape( $value ) . '" target="_blank" title="' . AttrEscape( $file_name . ' (' . $file_size . ')' ) . '"',
		'bigger'
	);
}



/**
 * Make Full Name
 * Add link to User Info/Student Info
 *
 * Local function
 * DBGet callback
 *
 * @param  string $value   Field value.
 * @param  string $name    'FULL_NAME'.
 *
 * @return string
 */
function _makePDFArchiveFullName( $value, $column )
{
	global $THIS_RET;

	if ( $value )
	{
		$profile = [
			'admin' => _( 'Administrator' ),
			'teacher' => _( 'Teacher' ),
			'parent' => _( 'Parent' ),
		];

		if ( ! AllowUse( 'Users/User.php' )
			|| empty( $THIS_RET['STAFF_ID'] )
			|| $THIS_RET['STAFF_SYEAR'] !== UserSyear() ) // User ID is not in current SYEAR...
		{
			if ( ! empty( $THIS_RET['PROFILE'] ) )
			{
				return '<span title="' . AttrEscape( $profile[ $THIS_RET['PROFILE'] ] ) . '">' .
					$value . '</span>';
			}

			return $value;
		}

		$url = 'Modules.php?modname=Users/User.php&staff_id=' . $THIS_RET['STAFF_ID'];

		return '<a href="' . URLEscape( $url ) . '" title="' .
			AttrEscape( $profile[ $THIS_RET['PROFILE'] ] ) . '">' . $value . '</a>';
	}

	if ( ! empty( $THIS_RET['FULL_NAME_STUDENT'] ) )
	{
		if ( ! AllowUse( 'Students/Student.php' )
			|| empty( $THIS_RET['STUDENT_ID'] ) ) // Should check if student enrolled in current SYEAR...
		{
			return '<span title="' . AttrEscape( _( 'Student' ) ) . '">' .
				$THIS_RET['FULL_NAME_STUDENT'] . '</span>';
		}

		$url = 'Modules.php?modname=Students/Student.php&student_id=' . $THIS_RET['STUDENT_ID'];

		return '<a href="' . URLEscape( $url ) . '" title="' . AttrEscape( _( 'Student' ) ) . '">' . $THIS_RET['FULL_NAME_STUDENT'] . '</a>';
	}

	return '';
}
