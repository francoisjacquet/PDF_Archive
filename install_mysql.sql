/**
 * Install MySQL
 * Required if the module has menu entries
 * - Add profile exceptions for the module to appear in the menu
 * - Add program config options if any (to every schools)
 * - Add module specific tables (and their eventual sequences & indexes)
 *   if any: see rosariosis.sql file for examples
 *
 * @package PDF Archive module
 */

/**
 * profile_exceptions Table
 *
 * profile_id:
 * - 0: student
 * - 1: admin
 * - 2: teacher
 * - 3: parent
 * modname: should match the Menu.php entries
 * can_use: 'Y'
 * can_edit: 'Y' or null (generally null for non admins)
 */
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'PDF_Archive/PDFArchive.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='PDF_Archive/PDFArchive.php'
    AND profile_id=1);


/**
 * program_config Table
 *
 * syear: school year (school may have various years in DB)
 * school_id: may exists various schools in DB
 * program: convention is plugin name, for ex.: 'pdf_archive'
 * title: for ex.: 'PDF_ARCHIVE_[your_config]'
 * value: string
 */
--
-- Data for Name: program_config; Type: TABLE DATA; Schema: public; Owner: rosariosis
--

INSERT INTO program_config (syear, school_id, program, title, value)
SELECT sch.syear, sch.id, 'pdf_archive', 'PDF_ARCHIVE_DELETE_OLDER_THAN_DAYS', '365'
FROM schools sch
WHERE NOT EXISTS (SELECT title
    FROM program_config
    WHERE title='PDF_ARCHIVE_DELETE_OLDER_THAN_DAYS');

INSERT INTO program_config (syear, school_id, program, title, value)
SELECT sch.syear, sch.id, 'pdf_archive', 'PDF_ARCHIVE_EXCLUDE_PRINT', 'Y'
FROM schools sch
WHERE NOT EXISTS (SELECT title
    FROM program_config
    WHERE title='PDF_ARCHIVE_EXCLUDE_PRINT');


/**
 * Add module tables
 */

/**
 * PDF Archive table
 */
--
-- Name: pdf_archive; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS pdf_archive (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    school_id integer NOT NULL,
    staff_id integer, -- Can be NULL
    FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
    student_id integer, -- Can be NULL
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    program_title text NOT NULL,
    document_path text NOT NULL,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);
